use bookstore;

select * from books;
select * from orders;
select * from customers;

insert into orders (customer_id,book_id,order_id)
values (2,4,1001),(2,5,1001),(3,4,1002);

-- cross join
SELECT * FROM customers c cross join orders o;

insert into orders (customer_id,book_id,order_id)
SELECT customers.id,books.id,1003 from customers cross join books where books.id=6;

select * from orders;

-- inner join 
select orders.order_id, customers.first_name, customers.last_name, books.title
from customers 
inner join orders on customers.id=orders.customer_id
inner join books on orders.book_id=books.id;

select o.order_id as "Order",c.first_name as "First Name", c.last_name as "Last Name",b.title as "Title"
from customers c
inner join orders o on c.id=o.customer_id
inner join books b on o.book_id=b.id;

use human_resources;
select * from employees;

select e.first_name  as "Manager first name" ,e.last_name as "Manager last name", m.first_name ,m.last_name
from employees e
inner join employees m
on e.employee_id=m.manager_id;

select e.first_name,e .last_name, m.first_name, m.last_name
from employees e, employees m
where e.employee_id=m.manager_id;

use bookstore;

select * from books;
select c.first_name,c.last_name, b.title
from books b, customers c, orders o
where c.id=o.customer_id  and o.book_id=b.id and b.author like'ion%';

select c.first_name,o.order_id from customers c
left join orders o 
on c.id=o.customer_id;

insert into customers values(null,"Gigel","Ion",null);

create database pets;
use pets;

create table owner(
id int primary key auto_increment,
name varchar(10) not null
);

create table pet(
id int primary key auto_increment,
name varchar(10) not null,
owner_id int,
constraint owner_fk foreign key(owner_id) references owner(id)
);

insert into owner (name) values ("Ion"),("Mihai"),("Gigel");
insert into pet (name,owner_id) values ("Rex",1),("Diesel",1),("Grivei",null);

select o.name,p.name
from owner o
inner join pet p
on o.id=p.owner_id;

select o.name,p.name from owner o, pet p where o.id=p.owner_id;

select o.name,p.name
from owner o
left join pet p
on o.id=p.owner_id;

select o.name,p.name
from owner o
join pet p
on o.id=p.owner_id;

select o.name,p.name
from owner o
cross join pet p;


select o.name,p.name
from owner o
left join pet p
on o.id=p.owner_id
union
select o.name,p.name
from owner o
right join pet p
on o.id=p.owner_id;











