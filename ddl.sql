-- delete a database
drop database sakila;
drop database if exists sakila;

-- create a database
create database gym;

-- connect to a database
use gym;

-- show all databases;
show databases;

-- create a table
create table trainers(
 first_name varchar(10),
 last_name varchar(10),
 specialities char(20)
);

-- show datas about tables
show tables;
describe trainers;
describe table trainers;

create table subscriptions(
start_date date,
end_date date 
);

create table if not exists subscriptions(
start_date date,
end_date date 
);

desc subscriptions;


-- add column to a table
alter table subscriptions 
add column customer_name varchar(10);

desc subscriptions;

-- delete a column from table
alter table subscriptions
drop column customer_name;

alter table subscriptions
add column wrong_name varchar(20);

-- change column name
alter table subscriptions
rename column wrong_name to type;

alter table subscriptions
modify column type enum('DAYTIME','FULLTIME');

create table test(
id int
);

create table test2(
id int
);

-- delete tables
DROP TABLE if exists test,test2;

desc subscriptions;

ALTER TABLE subscriptions
MODIFY COLUMN start_date DATE NOT NULL,
MODIFY COLUMN end_date DATE NOT NULL;

ALTER TABLE subscriptions
MODIFY COLUMN type ENUM('DAYTIME','FULLTIME') DEFAULT 'DAYTIME';

ALTER TABLE trainers
ADD COLUMN phone varchar(10);

ALTER TABLE trainers
MODIFY COLUMN phone VARCHAR(10) UNIQUE;

DESC trainers;

ALTER TABLE trainers
ADD COLUMN id INT PRIMARY KEY auto_increment;

ALTER TABLE subscriptions
ADD COLUMN id INT PRIMARY KEY auto_increment;

desc subscriptions;
 
 SHOW CREATE TABLE subscriptions;

create table customer (
id int primary key auto_increment,
first_name varchar(10) not null,
last_name varchar(10) not null,
email varchar(20) unique
);

alter table subscriptions
add constraint date_constraint CHECK(end_date>start_date);

show create table subscriptions;


create table classes(
id int primary key auto_increment,
name varchar(10) not null unique,
description text,
schedule datetime not null,
constraint check_date CHECK(schedule>'2020-01-01 00:00:00')
);

desc classes;
show create table classes;

desc customer;

alter table customer
modify column active tinyint(1) default 0;


alter table trainers
add constraint phone_length CHECK(length(phone)=10);

show create table trainers;

drop table subscriptions;





