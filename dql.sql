-- all trainers , all columns
SELECT * FROM trainers;

-- all trainers, only first_name and last_name columns displayed
SELECT first_name,last_name from trainers;

-- only trainers with first_name Sorin and only first_name/last_name columns displayed
SELECT first_name,last_name from trainers where first_name='Sorin';

select * from subscriptions;
insert into subscriptions 
values('2020-01-2','2020-02-01','Vali',null,null),
('2020-01-2','2020-02-01','Mihai',null,null),
('2020-02-2','2020-03-01','George',null,null),
('2020-02-2','2020-04-01','Andrei','FULLTIME',null);

-- =
SELECT * FROM subscriptions where start_date='2020-01-02';
SELECT * FROM subscriptions where customer_name='Ion';

-- != <>
SELECT customer_name from subscriptions where customer_name!='Ion';
SELECT id, customer_name from subscriptions where start_Date<>'2020-01-02';

-- >,<,>=,<=
SELECT customer_name from subscriptions where customer_name>='Ion';
SELECT * from subscriptions where id >4;

-- BETWEEN
SELECT id,customer_name from subscriptions where id between 3 and 5;
SELECT id,customer_name from subscriptions where id>=3 AND id<=5;

-- IS NULL, IS NOT NULL
select id,customer_name from subscriptions where id is not null;
select id from subscriptions where type is null;

select * from subscriptions;

-- IN, NOT IN
select * from subscriptions where id in(3,5);
select * from subscriptions where customer_name not in('Ion','Andrei');

-- LIKE
SELECT customer_name from subscriptions where customer_name like 'A%';
SELECT customer_name from subscriptions where customer_name like '_n%';
SELECT customer_name from subscriptions where customer_name like 'a_%';
SELECT customer_name from subscriptions where customer_name like '___'; -- clientii cu nume din 3 litere
SELECT customer_name from subscriptions where customer_name like '%ndre%';

--- AND, OR, NOT
SELECT * from subscriptions where start_date>'2020-01-02' and end_date<'2020-04-01';
SELECT * FROM subscriptions where (start_date>'2020-01-02' or start_Date<'2020-02-04') AND type is null;
select * from subscriptions where NOT customer_name='Ion';

-- AGGREGATE FUNCTIONS: MIN,MAX, AVG, COUNT, SUM
SELECT count(*) FROM subscriptions where start_date>='2020-02-01'; 
SELECT count(type) FROM subscriptions where start_date>='2020-02-01'; -- count only entries not null on type column

alter table subscriptions
add column price dec(10,4) not null;

update subscriptions set price=id*100;

update subscriptions set price=100.11111 where customer_name='Ion';

select price from subscriptions;

select sum(price) from subscriptions where price>=300;

select max(price) from subscriptions where customer_name<>'Ion';

select avg(price) from subscriptions;

use gym;
select * from subscriptions where start_date NOT BETWEEN '1980-01-01' and '2020-01-03';

select * from trainers where first_name not like 's%' and first_name not like 'T%';

select distinct start_date from subscriptions;







