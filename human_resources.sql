create database human_resources;
use human_resources;

create table employees (
employee_id int,
first_name varchar(20),
last_name varchar(20),
date_of_birth date,
postal_address varchar(30)
);

alter table employees
add column phone_number varchar(10),
add column email varchar(10),
add column salary int;

alter table employees 
drop column postal_address;

create table emp(
country varchar(10)
);

drop table emp;

insert into employees (employee_id,first_name,last_name,date_of_birth,phone_number,email,salary) 
values(1,'Jhon','Jhonson','1972-01-01','0800800314','@',100);

update employees set date_of_birth='1980-01-01' where first_name='John' and last_name='Jhonson';

delete from employees;
-- truncate employees

alter table employees 
modify column email varchar(30);

insert into employees (employee_id,first_name,last_name,date_of_birth,phone_number,email,salary) 
values(1, 'John', 'Johnson', '1975-01-01', '0800800888', 'john@johnson.com', 1000),
(2, 'James', 'Jameson', '1985-02-02', '0800800999', 'james@jameson.com', 2000);

select * from employees;
select first_name,last_name from employees;
select * from employees where last_name='Johnson';
select * from employees where last_name like 'J%';
select * from employees where last_name like '%_so_%';
select * from employees where date_of_birth>='1980-01-01';
select * from employees where year(date_of_birth)>=1980;
select * from employees where date_of_birth>='1980-01-01' and first_name='John';
select * from employees where date_of_birth>='1980-01-01' or first_name='John';
select * from employees where last_name<>'Jameson';

select min(salary),max(salary),avg(salary) from employees;

use human_resources;
desc employees;

alter table employees
add column manager_id int;

alter table employees
add constraint manager_fk foreign key(manager_id) references employees(employee_id);

select * from employees;

update employees set manager_id=4 where first_name in ('John','James');

create table project(
id int primary key auto_increment,
description varchar(30) not null
);

create table employee_project(
id int primary key auto_increment,
employee_id int not null,
project_id int not null,
constraint employee_fk foreign key(employee_id) references employees(employee_id)
on delete cascade,
constraint project_fk foreign key(project_id) references project(id)
on delete cascade
);

insert into project (description) 
values('Python - Cinema Web App'),('Java - Fitness Web App.');

select * from employees;

insert into employee_project (employee_id,project_id) 
values (1,1),(2,1),(3,2),(4,2);













