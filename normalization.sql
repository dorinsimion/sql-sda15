create database normalization;
use normalization;
-- bad design
create table receipts(
id int primary key auto_increment,
name varchar(20) not null,
description text,
ingredients text
);

insert into receipts (name,ingredients) 
values ("omleta","oua,sare,piper"),("clatite","oua,faina,lapte,apa");

select * from receipts;

-- 1NF

create table ingredients (
id int primary key auto_increment,
name varchar(15) not null
);

create table recipes (
id int primary key auto_increment,
name varchar(20),
description text
);

create table ingredients_recipes (
id int primary key auto_increment,
ingredient_id int not null,
recipe_id int not null,
constraint ingredient_fk foreign key(ingredient_id) references ingredients(id),
constraint recipe_fk foreign key(recipe_id) references recipes(id)
);

-- 2NF
create table students(
id int primary key auto_increment,
name varchar(20) not null,
class varchar(20) not null,
grade int not null,
constraint grade_check CHECK(grade between 1 and 10)
);

insert into students (name,class,grade) values("Ion","Matematica",10), ("Ion","Romana",9);

create table students2(
id int primary key auto_increment,
name varchar(20) not null
);
 
 drop table if exists classes;
 
create table classes(
id int primary key auto_increment,
name varchar(20) not null,
grade int not null,
student_id int not null,
constraint student_fk foreign key(student_id) references students2(id)
);


-- 3NF
create table customer(
id int  primary key auto_increment,
name varchar(20) not null,
city varchar(20) not null,
country varchar(20) not null
);

create table client(
id int primary key auto_increment,
name varchar(20) not null
);

create table location(
id int primary key auto_increment,
city varchar(20) not null,
country varchar(20) not null
);

create table client_location(
id int primary key auto_increment,
client_id int not null,
location_id int not null,
constraint client_fk foreign key(client_id) references client(id),
constraint location_fk foreign key(location_id) references location(id)
);





