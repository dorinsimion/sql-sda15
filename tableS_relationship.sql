-- one to one

create database table_relation;
use table_relation;

create table client(
id int primary key auto_increment,
username varchar(20) not null,
password varchar(20) not null
);

create table client_info (
id int primary key,
first_name varchar(10) not null,
last_name varchar(10) not null,
email varchar(20) not null unique,
constraint client_fk foreign key(id) references client(id)
);

-- not working because there is no client with id=1;
insert into client_info (first_name, last_name,email) values("Mihai","Ion", "@");

insert into client (username,password) values ("ion.mihai","password");
insert into client_info values (1,"Mihai","Ion","@");


create table student(
id int primary key auto_increment,
name varchar(20) not null
);

create table teacher(
id int primary key auto_increment,
name varchar(20) not null
);

create table student_teacher(
id int primary key auto_increment,
student_id int not null,
teacher_id int not null,
constraint student_fk foreign key(student_id) references student(id)
on delete cascade,
constraint teacher_fk foreign key(teacher_id) references teacher(id)
on delete cascade
);

insert into student (name) values("Ion"),("Ion"),("Mihai"),("George");
insert into teacher (name) values("T1"),("T2"),("T3"),("T4");

insert into student_teacher (student_id,teacher_id) values 
(1,2),(1,1),(1,3),(2,2),(2,3);

insert into student_teacher (student_id,teacher_id) values (5,1);

select * from student_Teacher;

delete from teacher where name="T2";


create table employees (
id int primary key auto_increment,
name varchar(20) not null,
department_id int,
manager_id int,
constraint manager_fk foreign key(manager_id) references employees(id)
);

 insert into employees (name,manager_id) values("Ion",null);
 insert into employees (name,manager_id) values("Mihai",1);
 
 delete from employees where id=1;
 

