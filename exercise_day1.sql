CREATE DATABASE bookstore;

use bookstore;

create table books(
id int primary key auto_increment,
author varchar(20) not null,
constraint check ( author like "% %" ),
title varchar(20) not null,
year date not null
);

 show create table books;
 
alter table books 
drop constraint books_chk_1;

alter table books
add constraint author_check check(author like '%_ _%');


create table customer(
id int primary key auto_increment,
first_name varchar(20),
last_name varchar(20),
phone varchar(10) unique,
constraint check (length(phone)=10)
);

show create table customer;

alter table customer
modify column first_name varchar(20) not null,
modify column last_name varchar(20) not null;

alter table customer 
drop constraint customer_chk_1,
add constraint phone_check CHECK(length(phone)=10);


create table orders(
id int primary key auto_increment,
customer_id int not	null,
book_id int not null,
order_id int not null
);


alter table books
add column price int not null,
add constraint price_check check(price between 10 and 100);

 -- insert 5 books, 2 of them same author (Ion Creanga) and any 3 of them have date in same year;
 -- insert 3 customers, one of them without a phone number;


INSERT INTO books (author,title,year,price) values
('Ion Creanga' , 'Amintiri copilarie' , '2000-01-01' , '50'),
('Ion Creanga' , 'Capra cu trei iezi' , '1995-02-04' , '30'),
('Mihai Eminescu' , 'Luceafarul' , '2000-04-02' , '60'),
('Ion Luca Caragiale' , 'Domnul Goe' , '1998-5-6' , '45'),
('Mihail Sadoveanu' , 'Baltagul' , '2000-04-02' , '55');

INSERT INTO CUSTOMER (first_name,last_name,phone) values('Marius', 'Dinca', '0731134196'),
('Andrei', 'Valahu', '0733331332'),('Leandro', 'Sanchez',null);


-- there is a tax for each book of 1 ron, increase all prices by 1
update books set price=price+1;

-- delete books cheaper than 35
DELETE FROM books where price<35;

-- display Ion Creanga books;
SELECT title FROM books where author='Ion Creanga';

-- average price of Ion Creanga books;
select avg(price) from books where author='Ion Creanga';

-- most expensive book;
SELECT MAX(price) from books;

-- first book released;
select min(year) from books;

-- min price of books from 2000
select min(price) from books where year(year)=2000;

-- custumers witout phone
select first_name,last_name from customer where phone is null;

use bookstore;
truncate orders;

insert into orders (customer_id,book_id,order_id) values(10,10,1001),(10,20,1001);

select * from customer where id=10;
select * from books where id in (10,20);

alter table customer
rename to customers;

truncate orders;
alter table orders
add constraint customer_fk foreign key(customer_id) references  customers(id),
add constraint book_fk foreign key(book_id) references books(id);

insert into orders (customer_id,book_id,order_id) values(1,3,1001);

select id from books;

delete from books where id=3;

alter table orders
drop constraint customer_fk,
drop constraint book_fk;

alter table orders
modify column book_id int null;

alter table orders
add constraint book_fk foreign key(book_id) references books(id)
on delete set null
on update cascade;

delete from books where id=3;

select * from orders;

alter table orders
add constraint customer_fk foreign key(customer_id) references customers(id)
on delete cascade
on update cascade;

select * from orders;
update customers set id=100 where id=1;

delete from customers where id=100;

use human_resources;
desc employees;

alter table employees
modify column employee_id int primary key auto_increment;

insert into employees (first_name,last_name,date_of_birth,phone_number,email,salary) values
('Julie', 'Juliette', '1990-01-01', '0800900111', 'julie@juliette.com', 5000),
('Sofie', 'Sophia', '1987-02-03', '0800900222', 'sofie@sophia.com', 1700);

select * from employees;
delete from employees where employee_id in (5,6);


create table departments(
department_id int primary key auto_increment,
name varchar(10) not null
);

desc departments;

insert into departments (name) values ('HR'),('FINANCE');

SELECT * FROM DEPARTMENTS;

ALTER TABLE EMPLOYEES 
ADD COLUMN DEPARTMENT_ID INT;

update employees set  department_id=1 where employee_id=1;
update employees set  department_id=2 where employee_id=4;

delete from departments where name='HR';

ALTER TABLE EMPLOYEES
ADD CONSTRAINT department_fk foreign key(department_id) references departments(department_id);

insert into departments values(1,'HR');

delete from departments where name='HR';

insert into employees (first_name,last_name,date_of_birth,phone_number,email,salary,department_id) values
('xyz', 'Juliette', '1990-01-01', '0800900111', 'julie@juliette.com', 5000,1);

delete from employees where first_name='xyz';




