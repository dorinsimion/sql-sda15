-- insert into table
INSERT INTO trainers VALUES('Ion','Mihai','TRX',null,1);

INSERT INTO trainers VALUES('George','Mihai','YOGA',null,null);
desc trainers;

INSERT INTO trainers (first_name,last_name) VALUES ('George','Ion');
INSERT INTO trainers (last_name,first_name) VALUES ('Adrian','Sorin'),('Andrei','Ion');


-- delete from table
DELETE FROM trainers WHERE id=8;
DELETE FROM trainers where first_name='ion';

use gym;

DELETE FROM trainers;

-- ddl instruction
TRUNCATE trainers;

desc trainers;
-- modify rows from table
UPDATE trainers SET specialities='TRX';

UPDATE trainers SET phone='0724111222' WHERE first_name='Sorin';

-- error because phone defined as varchar(10)
UPDATE trainers SET phone='072411122212' WHERE first_name='Sorin';

-- error because constraint not met
UPDATE trainers SET phone='07241112' WHERE first_name='Sorin';

